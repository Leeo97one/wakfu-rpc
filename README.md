# WAKFU Rich Presence

![alt text](preview.png "Aperçu")

## Utilisation

L'installation d'une version récente de _Python_ et _pip_ est requise pour installer et utiliser ce programme.

 1. Cloner le dépôt avec git (ou récupérer une archive du code source via GitLab)
 2. Installer les dépendances avec pip : `pip install -r requirements.txt`
 3. Dans le répertoire `wakfu_rpc` : créer une copie du fichier `config.py.exemple` nommée `config.py`
 4. (Optionnel) Modifier la configuration située dans `config.py`
 5. Éxecuter le paquet avec `python -m wakfu_rpc`