from setuptools import setup, find_packages

setup(
    name='wakfu-rpc',
    version='0.0.2',
    packages=find_packages(),
    install_requires=[
        'pypresence',
        'requests',
        'watchdog'
    ],
    url='https://gitlab.com/Leeo97one/wakfu-rpc',
    license='',
    author='Leeo97one',
    author_email='contact.leeo97one@gmail.com',
    description='WAKFU Rich Presence pour Discord'
)
