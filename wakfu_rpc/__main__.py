import logging
import os
import time

from watchdog.observers import Observer

from wakfu_rpc import WakfuRpc, paths, config
from wakfu_rpc.events import AccountPreferencesEventHandler, SettingsEventHandler

logging.basicConfig(level=logging.DEBUG)

wakfu_rpc = WakfuRpc()
wakfu_rpc.update_rpc()

observer = Observer()

preferences_event_handler = AccountPreferencesEventHandler(wakfu_rpc)
observer.schedule(preferences_event_handler, paths.wakfu_preferences_dir())

if config.PROFILE_URL is None:
    settings_event_handler = SettingsEventHandler(wakfu_rpc)
    observer.schedule(settings_event_handler, paths.zaap_dir())
    wakfu_rpc.update_profile_url(os.path.join(paths.zaap_dir(), 'Settings'))
else:
    wakfu_rpc.profile_url = config.PROFILE_URL

observer.start()

try:
    while observer.is_alive():
        time.sleep(config.REFRESH_INTERVAL)
        preferences_event_handler.wakfu_rpc.refresh_from_profile()
        preferences_event_handler.wakfu_rpc.update_rpc()
except KeyboardInterrupt:
    observer.stop()

observer.join()
preferences_event_handler.wakfu_rpc.rpc.close()
