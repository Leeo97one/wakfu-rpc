import json
import time

import requests
from pypresence import Presence

from wakfu_rpc import config


def get_discord_rpc(client_id):
    rpc = Presence(client_id)
    rpc.connect()

    return rpc


class WakfuRpc:
    class_image_key = {
        'Crâ': 'cra',
        'Ecaflip': 'ecaflip',
        'Eliotrope': 'eliotrope',
        'Eniripsa': 'eniripsa',
        'Enutrof': 'enutrof',
        'Féca': 'feca',
        'Huppermage': 'huppermage',
        'Iop': 'iop',
        'Osamodas	': 'osamodas',
        'Ouginak': 'ouginak',
        'Pandawa': 'pandawa',
        'Roublard': 'roublard',
        'Sacrieur': 'sacrieur',
        'Sadida': 'sadida',
        'Sram': 'sram',
        'Steamer': 'steamer',
        'Xélor': 'xelor',
        'Zobal': 'zobal'
    }

    def __init__(self):
        self.rpc = get_discord_rpc(config.CLIENT_ID)
        self.start = time.time()
        self.profile_url = "??"
        self.character_name = "??"
        self.level = "??"
        self.server_name = "??"
        self.class_name = "??"

    def refresh_from_profile(self):
        r = requests.get(self.profile_url + '/wakfu', timeout=5)
        r.raise_for_status()

        # Méthode immonde pour récupérer des infos dans un code HTML immonde.
        for line in r.text.splitlines():
            if "</span>" + self.character_name + "</td>" in line:
                line = line.split("<td>")
                self.class_name = line[2].strip("</td> ")
                if self.class_name == 'Cra':
                    # Merci de pas écorcher le nom de la meilleure classe du jeu 🙃
                    self.class_name = 'Crâ'
                self.level = line[3].strip("Lvl </td>")
                self.server_name = line[4].strip("</td> </tr>")
                break
        else:
            self.class_name = "??"
            self.level = "??"
            self.server_name = "??"

    def update_profile_url(self, settings_path):
        with open(settings_path, 'r') as file:
            settings = json.load(file)
            self.profile_url = 'https://account.ankama.com/fr/profil-ankama/' + settings['USER_INFO']['nickname']

    def update_rpc(self):
        self.rpc.update(
            state=self.character_name,
            details="Niveau " + self.level,
            start=int(self.start),
            large_image='wakfu',
            large_text=self.server_name,
            small_image=self.class_image_key.get(self.class_name),
            small_text=self.class_name
        )
