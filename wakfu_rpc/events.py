import logging

from watchdog.events import PatternMatchingEventHandler, FileSystemEventHandler


class AccountPreferencesEventHandler(PatternMatchingEventHandler):

    def __init__(self, wakfu_rpc):
        super().__init__(("*accountPreferences*.properties",), ignore_directories=True, case_sensitive=True)
        self.wakfu_rpc = wakfu_rpc

    def on_any_event(self, event):
        logging.debug(event)
        with open(event.src_path, 'r') as file:
            for line in file:
                if line.startswith("lastSelectedCharacterName"):
                    # .encode().decode('unicode-escape') : pour décoder correctement les caractères UTF-8
                    # éventuellement présents (ex. : H\u00E9l\u00E9o → Héléo).
                    character_name = line.split("=")[1].strip().encode().decode('unicode-escape')
                    if self.wakfu_rpc.character_name == character_name:
                        return
                    self.wakfu_rpc.character_name = character_name
                    break
            else:
                self.wakfu_rpc.character_name = "??"

        self.wakfu_rpc.refresh_from_profile()
        self.wakfu_rpc.update_rpc()


class SettingsEventHandler(FileSystemEventHandler):
    def __init__(self, wakfu_rpc):
        self.wakfu_rpc = wakfu_rpc

    def on_any_event(self, event):
        logging.debug(event)
        self.wakfu_rpc.update_profile_url(event.src_path)

    def dispatch(self, event):
        if not event.is_directory and event.src_path.endswith('Settings'):
            super(SettingsEventHandler, self).dispatch(event)
