import os
import sys

from wakfu_rpc import config


def zaap_dir():
    if config.PREFERENCES_PATH is not None:
        return config.PREFERENCES_PATH

    if sys.platform.startswith('linux'):
        config_dir = os.getenv('XDG_CONFIG_HOME', os.path.join(os.getenv('HOME'), '.config'))
    elif sys.platform == 'win32':
        config_dir = os.getenv('APPDATA')
    elif sys.platform == 'darwin':
        # TODO
        return None
    else:
        return None

    if os.path.exists(os.path.join(config_dir, 'zaap')):
        return os.path.join(config_dir, 'zaap')
    elif os.path.exists(os.path.join(config_dir, 'zaap-steam')):
        return os.path.join(config_dir, 'zaap-steam')
    else:
        return None


def wakfu_preferences_dir():
    return os.path.join(zaap_dir(), 'wakfu', 'preferences')
